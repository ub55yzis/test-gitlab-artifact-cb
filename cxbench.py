#!/usr/bin/env python

import getopt
import sys, os
import re
import os.path
from socket import gethostname 
from string import Template
from locale import getpreferredencoding
from base64 import standard_b64encode

ENCODING = getpreferredencoding()

html_template = Template("""
<!doctype html>
<html>
 
 <body>
  <h1>$title</h1>
  <div id="$slug" class="plotly-graph-div" style="height:100%; width:100%;"></div>
 <script src='https://cdn.plot.ly/plotly-latest.min.js'></script>
 <script type="text/javascript">

    (function(){
                window.PLOTLYENV={'BASE_URL': 'https://plotly.com'};

                var gd = document.getElementById('$slug')
                var resizeDebounce = null;

                function resizePlot() {
                    var bb = gd.getBoundingClientRect();
                    Plotly.relayout(gd, {
                        width: bb.width,
                        height: bb.height
                    });
                }
                Plotly.plot(gd, {
                    data: [{"line": {"dash": "solid", "color": "#636efa"},
                            "mode": "lines",
                            "type": "scatter",
                            "x": [$xdata],
                            "y": [$ydata],
                            "text": [$hoverdata],
                            "xaxis": "x",
                            "yaxis": "y",
                            "hoverlabel": {"namelength": 0},
                            "showlegend": false,
                            "hovertemplate": "Date=%{x}<br>Performance=%{y}<br>%{text}"
                           }],
                    layout: {},
                    frames: [],
                    config: {"showLink": false}
                });
                
           }());
 </script>
 </body>
</html>
""")

def get_table_entries(line, key):
    out = []
    regex = "\s*\"{}\"\s*:\s*\[(.+)\]".format(key)
    m = re.match(regex, line)
    if m:
        values = m.group(1).split(",")
        for v in values:
            if "\"" in v:
                out.append(v.strip().strip("\""))
            else:
                out.append(float(v))
    return out


def read_inputs(filename):
    inputx = []
    inputy = []
    inputh = []
    title = None
    slug = None
    if filename and os.path.exists(filename):
        with open(filename) as fp:
            d = fp.read()
            if type(d) == 'bytes': d = d.decode(ENCODING)
            for l in fp.read().split("\n"):
                m = re.match("\s*<h1>(.+)</h1>", l)
                if m:
                    title = m.group(1).strip()
                m = re.match("\s*<div id=\"([^\"]+)\" ", l)
                if m:
                    slug = m.group(1).strip()
                x = get_table_entries(l, "x")
                if len(x) > 0: inputx = x
                y = get_table_entries(l, "y")
                if len(y) > 0: inputy = y
                h = get_table_entries(l, "text")
                if len(h) > 0: inputh = h
    return title, slug, inputx, inputy, inputh

opts = []
args = []
try:
    opts, args = getopt.getopt(sys.argv[1:], 'hf:a:c:b:r:p:d:k:t:', ["file=", "author=", "commit=", "branch=", "routine=", "performance=", "date=", "keep=", "hostname=", "title=", "namespace=", "project="])
except getopt.GetoptError as err:
    print(err)
    sys.exit(2)

html_filename = None
author = os.getenv("CI_COMMIT_AUTHOR", None)
commit = os.getenv("CI_COMMIT_SHA", None)
branch = os.getenv("CI_COMMIT_REF_NAME", None)
routine = None
date = os.getenv("CI_COMMIT_TIMESTAMP", None)
namespace = os.getenv("CI_PROJECT_NAMESPACE", "Unknown")
project = os.getenv("CI_PROJECT_NAME", "Unknown")
performance = None
title = None
keep = 10
hostname = gethostname()
for o, a in opts:
    if o in ("-h", "--help"):
        print("Bla")
        sys.exit()
    elif o in ("-f", "--file"):
        html_filename = a
    elif o in ("-a", "--author"):
        author = a
    elif o in ("-c", "--commit"):
        commit = a
    elif o in ("-b", "--branch"):
        branch = a
    elif o in ("-r", "--routine"):
        routine = a
    elif o in ("-d", "--date"):
        date = a
    elif o in ("-t", "--title"):
        title = a
    elif o in ("-k", "--keep"):
        keep = int(a)
    elif o in ("-p", "--performance"):
        performance = float(a)
    elif o in ("--hostname"):
        hostname = a
    elif o in ("--namespace"):
        namespace = a
    elif o in ("--project"):
        project = a

if not (date and performance):
    print("Date and performance are required at least")
    sys.exit(1)

t,s,x,y,h = read_inputs(html_filename)
if len(x) > keep:
    x = x[(len(x)-keep):]
    y = y[(len(y)-keep):]
    h = h[(len(h)-keep):]

x.append(date)
y.append(performance)
hover = ""
if commit: hover += "Commit: {}<br>".format(commit)
if author: hover += "Autor: {}<br>".format(author)
if branch: hover += "Branch: {}<br>".format(branch)
if routine: hover += "Routine: {}<br>".format(routine)
if hostname: hover += "Hostname: {}<br>".format(hostname)
h.append(hover)

if title: t = title
if not t:
    t = "Performance measurements for project {}/{}".format(namespace, project)
if not s:
    bs = standard_b64encode(bytes(t, ENCODING))
    s = str(bs, ENCODING)


repl = {"xdata" : ", ".join(["\"{}\"".format(i) for i in x]),
        "ydata" : ", ".join([str(i) for i in y]),
        "hoverdata" : ", ".join(["\"{}\"".format(i) for i in h]),
        "title" : t,
        "slug" : s}

if html_filename:
    with open(html_filename, "w") as fp:
        fp.write(html_template.substitute(repl))
else:
    print(html_template.substitute(repl))
